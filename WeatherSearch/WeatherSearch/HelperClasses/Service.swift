//
//  Service.swift
//  WeatherSearch
//
//  Created by Asmaa Badreldin on 7/8/21.
//

import Foundation
import Alamofire

struct Service {
    
    static let shared = Service()
    
    // generic func to Reuse in all Api Calls
    func fetchGenericData<T: Decodable>(urlString: String,methodUsed:HTTPMethod, params: [String : Any]? , successBlock: @escaping (T) -> (),faliureBlock: @escaping (Error?) -> ()) {
        guard let url = URL(string: urlString) else { return }
        let headers = ["Accept":"application/json","Content-Type":"application/json"]
        Alamofire.request(url, method:methodUsed, parameters: params,encoding: JSONEncoding.default, headers:headers).responseJSON { response in
            switch response.result {
            case .success:
                let jsonDecoder = JSONDecoder()
                do {
                    guard let data = response.data else { return }
                    let dataFeed = try jsonDecoder.decode(T.self, from: data)
                    
                    DispatchQueue.main.async {
                       // print(dataFeed)
                        successBlock(dataFeed)
                    }
                } catch let jsonErr {
                    print("Failed to serialize json:", jsonErr.localizedDescription)
                    faliureBlock(jsonErr)
                }
            case .failure:
                faliureBlock(response.error)
            }
        }
    }
}

//
//  NSUserDefaultHandler.swift
//  WeatherSearch
//
//  Created by Asmaa Badreldin on 7/11/21.
//

import Foundation
import UIKit

class NSUserDefaultHandler: NSObject {
    
    // save rate application Date
    static func userCountryAdded (added: Bool){
        UserDefaults.standard.set(true, forKey: "cityAdded")
    }
    
    // retrieve Rate Application Date To Show
    static func CheckUserCountryAdded() -> Bool?{
        let added:Bool = UserDefaults.standard.bool(forKey: "cityAdded")
        return added
    }

}

//
//  DismissDelegate.swift
//  WeatherSearch
//
//  Created by Asmaa Badreldin on 7/11/21.
//

import Foundation
import UIKit

protocol DismissViewControlerDelegate {
    func dismissVC()
}

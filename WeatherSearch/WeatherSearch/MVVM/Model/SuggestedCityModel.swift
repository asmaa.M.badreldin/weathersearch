//
//  SuggestedCityModel.swift
//  WeatherSearch
//
//  Created by Asmaa Badreldin on 7/10/21.
//

import Foundation
import RealmSwift

class SuggestedCityModel: Object {
    @objc dynamic var name: String? = nil
    @objc dynamic var isAddedToMain: Bool = false
}

//
//  SearchCityWeatherVC.swift
//  WeatherSearch
//
//  Created by Asmaa Badreldin on 7/9/21.
//

import UIKit

class SearchCityWeatherVC: ParentVC {

    //MARK:- Outlets
    @IBOutlet weak var searchResultTableView: UITableView!
    @IBOutlet weak var CityNameLbl: UILabel!
    
    //MARK:- Properties
    var shouldShowSearchResults = true // show search suggestion or search result

    lazy var searchCityVM : SearchCityVM = {
        return SearchCityVM()
    }()
    
    var comesFromSearchIcon = true
    var citySelected: String = ""

    // table search activity controller
    let activityIndicator = UIActivityIndicatorView(style: .gray)

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        designHandler()
        activityIndConfiguration()
        savedDataHandler()
        
        if comesFromSearchIcon{
            configureSearchController()
        }else{
            if citySelected.isEmpty == false{
                self.searchCityVM.searchKey = citySelected
                self.searchApiHandler()
            }
        }
    }
        
    @IBAction func backBtnHandler(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}

//MARK:- Design handlers
extension SearchCityWeatherVC{
    func designHandler(){
        searchResultTableView.register(UINib(nibName: searchCityVM.forcastCellIdentifier, bundle: nil), forCellReuseIdentifier: searchCityVM.forcastCellIdentifier)
        
        searchResultTableView.register(UINib(nibName: searchCityVM.suggestedCellIdentifier, bundle: nil), forCellReuseIdentifier: searchCityVM.suggestedCellIdentifier)
    }
    
    func activityIndConfiguration(){
        activityIndicator.frame = CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0)
        activityIndicator.hidesWhenStopped = true
        activityIndicator.center = self.view.center
        self.view.addSubview(activityIndicator)
        activityIndicator.stopAnimating()
    }
    
    
    func endIndicatorsAnimation(){
        // main search indicator while first load search result
        self.activityIndicator.stopAnimating()
        // paging indicator loader -- stop it to insure that no indicators running
    }
    
    func configureSearchController(){
        let searchBar:UISearchBar = UISearchBar()
        searchBar.showsSearchResultsButton = true
        searchBar.placeholder = "search City"
        searchBar.sizeToFit()
        searchBar.isTranslucent = false
        searchBar.delegate = self
        searchResultTableView.tableHeaderView = searchBar
    }
}

//MARK:-UISearchBarDelegate
extension SearchCityWeatherVC:UISearchBarDelegate{
        
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
    }
    
    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        return true
    }

    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        print("searchBarTextDidEndEditing")
        print("searchBarSearchButtonClicked")
        // begin new search

        endIndicatorsAnimation()
        
        if searchCityVM.searchKey.isEmpty == false{
            // update search result
            shouldShowSearchResults = true
            searchResultTableView.reloadData()

            // call the api
            searchApiHandler()
        }
    }
       
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard let text = searchBar.text else {
            searchCityVM.searchKey = ""
            return
        }
        
        searchCityVM.searchKey = text
        print("search text :- \(text)")
    }

    public func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        endIndicatorsAnimation()
        
        shouldShowSearchResults = false
        searchResultTableView.reloadData()
    }
}

// MARK: - Api Loader Methods and data handler
extension SearchCityWeatherVC{
    //  api handle search result
    func searchApiHandler(){
        // show activity indicator in first load of data
        
        self.activityIndicator.startAnimating()
        
        self.searchCityVM.getSearchResult( completion: { (isSucess,errorMsg) in
            self.endIndicatorsAnimation()
            
            DispatchQueue.main.async {
                if isSucess{
                    // reload table with new result
                    self.searchResultTableView.reloadData()
                    self.endIndicatorsAnimation()
                    self.searchCityVM.saveSuggestedCities(searchWord: self.searchCityVM.searchKey)
                    self.savedDataHandler()
                }else{
                    self.endIndicatorsAnimation()
                    if errorMsg.isEmpty == false{
                        self.showToast(message: errorMsg, height: 100)
                    }
                }
            }
        })
    }
    
    func savedDataHandler(){
        self.searchCityVM.comesFrom = ScreenNameEnum.search.rawValue
        self.searchCityVM.getSavedSuggestedCities()
    }
}

// MARK: - TableView Delegate and DataSource Methods
extension SearchCityWeatherVC:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if shouldShowSearchResults{
            // forecast table count
            return self.searchCityVM.getListCount()
        }else{
            // suggestion table count
            return self.searchCityVM.getSuggestedListCount()
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if shouldShowSearchResults{
            let cell = tableView.dequeueReusableCell(withIdentifier: searchCityVM.forcastCellIdentifier) as! Forecastcell
            
            CityNameLbl.text = searchCityVM.searchKey
            
            // load cell with data
            searchCityVM.configCell(view: cell, indexPath: indexPath)
            
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: searchCityVM.suggestedCellIdentifier) as! SuggestedCityCell
            
            CityNameLbl.text = "Search City"
            
            searchCityVM.comesFrom = ScreenNameEnum.search.rawValue
            // load cell with data
            searchCityVM.configSuggestedCell(view: cell, indexPath: indexPath)
            
            cell.delegate = self

            return cell
        }
    }
}

//MARK:- suggestedCellDelegate
extension SearchCityWeatherVC:suggestedCellDelegate{
    func suggestCellIsclicked(cell: SuggestedCityCell, sender: UIButton) {
        // get selected indexPath
        if let indexPath = searchResultTableView.indexPath(for: cell){
            // update search bar text field data
            let searchTxt = searchCityVM.savedSuggestions[indexPath.row].name
            searchCityVM.searchKey = searchTxt ?? ""
            
            shouldShowSearchResults = true
            self.searchApiHandler()
            self.view.endEditing(true)
        }
    }
    
    func addSuggestedCityToMain(cell: SuggestedCityCell, sender: UIButton) {
        searchCityVM.addSuggestedCityToMain(city: cell.cityNameLbl.text ?? "")
        self.searchResultTableView.reloadData()
    }
}

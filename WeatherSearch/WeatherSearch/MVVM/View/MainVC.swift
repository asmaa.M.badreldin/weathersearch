//
//  MainViewController.swift
//  WeatherSearch
//
//  Created by Asmaa Badreldin on 7/8/21.
//

import UIKit
import CoreLocation

class MainVC: ParentVC {
    
    //MARK:- Outlets
    @IBOutlet weak var cityTableView: UITableView!
    
    //MARK:- Properties
    lazy var searchCityVM : SearchCityVM = {
        return SearchCityVM()
    }()

    var locationManager = CLLocationManager()

    override func viewDidLoad() {
        super.viewDidLoad()
        designHandler()
        
        let added = NSUserDefaultHandler.CheckUserCountryAdded()
        if added == false{
            locationMangerSetUp()
        }
        
        checkLocationAccess()
        
        savedDataHandler()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        dismissVC()
    }
    
    @IBAction func searchBtnHandler(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Search", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "SearchCityWeatherVC") as! SearchCityWeatherVC
        self.present(vc, animated: true, completion: nil)
    }
    
    func dismissVC() {
        savedDataHandler()
        self.cityTableView.reloadData()
    }
    
    // check if app fetch
    func locationMangerSetUp(){
        //Location Manager code to fetch current location
        self.locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
    }
    
    func checkLocationAccess(){
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                print("No access")
                let added = NSUserDefaultHandler.CheckUserCountryAdded()
                if added == false{
                    self.searchCityVM.comesFrom = ScreenNameEnum.main.rawValue
                    self.searchCityVM.saveSuggestedCities(searchWord: "London, UK")
                    self.searchCityVM.addSuggestedCityToMain(city: "London, UK")
                    NSUserDefaultHandler.userCountryAdded(added: true)
                    self.savedDataHandler()
                    self.cityTableView.reloadData()
                }
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
            @unknown default:
                break
            }
        } else {
            print("Location services are not enabled")
        }
    }
}

//MARK:- Design handlers
extension MainVC{
    func designHandler(){
        cityTableView.register(UINib(nibName: searchCityVM.suggestedCellIdentifier, bundle: nil), forCellReuseIdentifier: searchCityVM.suggestedCellIdentifier)
    }
}

//MARK:- Data handlers
extension MainVC{
    func savedDataHandler(){
        self.searchCityVM.comesFrom = ScreenNameEnum.main.rawValue
        self.searchCityVM.getSavedSuggestedCities()
    }
}

// MARK: - TableView Delegate and DataSource Methods
extension MainVC:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // suggestion table count
        return self.searchCityVM.getSuggestedListCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: searchCityVM.suggestedCellIdentifier) as! SuggestedCityCell
        
        searchCityVM.comesFrom = ScreenNameEnum.main.rawValue

        // load cell with data
        searchCityVM.configSuggestedCell(view: cell, indexPath: indexPath)
        
        cell.delegate = self
        
        return cell
    }
}

//MARK:- suggestedCellDelegate
extension MainVC:suggestedCellDelegate{
    func suggestCellIsclicked(cell: SuggestedCityCell, sender: UIButton) {
        // get selected indexPath
        let storyBoard = UIStoryboard(name: "Search", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "SearchCityWeatherVC") as! SearchCityWeatherVC
        vc.citySelected = cell.cityNameLbl.text ?? ""
        vc.comesFromSearchIcon = false
        self.present(vc, animated: true, completion: nil)
    }
    
    func addSuggestedCityToMain(cell: SuggestedCityCell, sender: UIButton) {
        self.searchCityVM.removeCityFromMain(searchWord: cell.cityNameLbl.text ?? "")
        savedDataHandler()
        self.cityTableView.reloadData()
    }
}

// MARK: - CLLocationManagerDelegate
extension MainVC:CLLocationManagerDelegate{
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]){
        guard let location = locations.last else{
            return
        }
        
        if location.coordinate.latitude != 0.0 && location.coordinate.longitude != 0.0{
            //Finally stop updating location otherwise it will come again and again in this delegate
            self.locationManager.stopUpdatingLocation()
            locationManager.delegate = nil
            
            fetchCityAndCountry(from: CLLocation(latitude: location.coordinate.latitude ?? 0.0, longitude: location.coordinate.longitude ?? 0.0)) { city, country, error in
                guard let city = city, let country = country, error == nil else {
                    return
                }
                
                self.searchCityVM.comesFrom = ScreenNameEnum.main.rawValue
                self.searchCityVM.saveSuggestedCities(searchWord: city)
                self.searchCityVM.addSuggestedCityToMain(city: city)
                NSUserDefaultHandler.userCountryAdded(added: true)
                self.savedDataHandler()
                self.cityTableView.reloadData()
            }
            
        }
    }
    
    func fetchCityAndCountry(from location: CLLocation, completion: @escaping (_ city: String?, _ country:  String?, _ error: Error?) -> ()) {
        CLGeocoder().reverseGeocodeLocation(location) { placemarks, error in
            completion(placemarks?.first?.administrativeArea,
                       placemarks?.first?.country,
                       error)
        }
    }

}

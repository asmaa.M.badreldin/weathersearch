//
//  ParentVC.swift
//  WeatherSearch
//
//  Created by Asmaa Badreldin on 7/8/21.
//

import UIKit

class ParentVC: UIViewController {

    override func viewDidLoad(){
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func showToast(message : String ,height:Int) {
        let toastLabel = UILabel(frame: CGRect(x: 50, y: self.view.frame.size.height/2, width: self.view.frame.size.width - 100, height: CGFloat(height)))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center
        toastLabel.numberOfLines = 0
        toastLabel.font = UIFont(name: "GESSTwoLight-Light", size: 12.0)
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 5.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
}

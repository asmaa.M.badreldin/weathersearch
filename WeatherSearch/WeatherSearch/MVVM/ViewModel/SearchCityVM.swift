//
//  SearchCityVM.swift
//  WeatherSearch
//
//  Created by Asmaa Badreldin on 7/8/21.
//

import Foundation
import RealmSwift

class SearchCityVM {
    
    init() {}
    
    var searchModel:SearchModel!
    
    var searchKey = ""
    
    let forcastCellIdentifier = "Forecastcell"
    let suggestedCellIdentifier = "SuggestedCityCell"

    var daysForcastList: [DaysForcast] = []
    var savedSuggestions:[SuggestedCityModel] = [SuggestedCityModel]()

    var comesFrom:Int = 0
    
    //MARK: - return Search Result
    func getSearchResult( completion : @escaping(Bool, String) -> Void){
        
        let urlParams = "q=\(searchKey)&apiKey=\(appKey)&lang=ar"
        
        let urlString = URls.searchCityCUrl + urlParams
        
        
        daysForcastList = []
        
        Service.shared.fetchGenericData(urlString: urlString, methodUsed: .get, params: nil, successBlock: { (responseModel: SearchModel) in
            if responseModel.cod == "200"{ // success
                if responseModel.list?.count != 0 {
                    
                    // set response
                    self.searchModel = responseModel
                    
                    // check if there is returned hits
                    if let returnedList = self.searchModel.list{
                        if returnedList.count > 0{ // check for count of articles
                            // append new result to old data
                            self.daysForcastList.append(contentsOf: returnedList)
                            
                            completion(true, "")
                        }else{
                            completion(false, "")
                        }
                    }
                    
                }else{
                    completion(false, "")
                }
            }else{
                completion(false, "")
            }
        }) { (error) in
            print("Errrrrror : " + (error?.localizedDescription)!)
            completion(false, (error?.localizedDescription)!)
        }
    }
    
    //  return list count
    func getListCount() -> Int {
        return daysForcastList.count
    }

    //  return config cell
    func configCell(view: Forecastcell, indexPath: IndexPath)  {
        if self.daysForcastList.count > 0{
            let obj = self.daysForcastList[indexPath.row]
            view.setData(dayForecast: obj)
        }
    }
    
    //MARK: - save and get suggestions from recent user search
    func saveSuggestedCities(searchWord:String){
        let savedSuggestions = RealmService.shared.getSavedSuggestions()
        // to be sure that value saved one time no duplication
        let predicate = NSPredicate(format: "name contains[cd] %@",searchWord)
        
        let results = savedSuggestions.filter(predicate)
 
        // if word already saved
        if results.count > 0{
            return
        }
        
        // save latest 15 search cities
        if savedSuggestions.count < 15{
            let obj = SuggestedCityModel()
            obj.name = searchWord
            RealmService.shared.addData(object: obj)
        }else{
            // already saved 15 suggestions as required
            // so update the first one
            if let firstObj = savedSuggestions.first{
                let dict: [String: Any?] = ["name": searchWord]
                RealmService.shared.update(firstObj, with: dict)
            }
        }
    }
    
    func addSuggestedCityToMain(city:String){
        let savedSuggestions = RealmService.shared.getSavedSuggestions()
        
        // to be sure that value saved one time no duplication
        let predicate = NSPredicate(format: "name contains[cd] %@ AND isAddedToMain == \(1)",city)
        let results = savedSuggestions.filter(predicate)
        // if word already saved and added to main
        if results.count > 0{
            return
        }
        
        // to be sure that value saved one time no duplication
        let predicate2 = NSPredicate(format: "isAddedToMain == \(1)")
        let addedToMainArr = savedSuggestions.filter(predicate2)
        
        // addedToMain < 5
        if addedToMainArr.count < 5{
            // get obj in suggested Arr
            let cityPredicate = NSPredicate(format: "name contains[cd] %@",city)
            let results = savedSuggestions.filter(cityPredicate)
            if let firstObj = results.first{
                let dict: [String: Any?] = ["isAddedToMain": 1]
                RealmService.shared.update(firstObj, with: dict)
            }
        }
    }
    
    //remove city from main
    func removeCityFromMain(searchWord:String){
        let savedSuggestions = RealmService.shared.getSavedSuggestions()
        // to be sure that value saved one time no duplication
        let predicate = NSPredicate(format: "name contains[cd] %@",searchWord)
        
        let results = savedSuggestions.filter(predicate)
        
        // if word already saved
        if results.count < 0{
            return
        }else{
            if let firstObj = results.first{
                let dict: [String: Any?] = ["isAddedToMain": 0]
                RealmService.shared.update(firstObj, with: dict)
            }
        }
    }
    
    // get saved suggestions
    func getSavedSuggestedCities(){
        savedSuggestions.removeAll()
        
        if comesFrom == ScreenNameEnum.search.rawValue{
            let savedData  = RealmService.shared.getSavedSuggestions()
            savedSuggestions.append(contentsOf: savedData)
        }else{
            let savedData  = RealmService.shared.getAddedCitiesToMain()
            savedSuggestions.append(contentsOf: savedData)
        }
    }
    
    // return suggested list count
    func getSuggestedListCount() -> Int {
        return savedSuggestions.count
    }
    
    // return suggested config cell
    func configSuggestedCell(view: SuggestedCityCell, indexPath: IndexPath)  {
        let obj = self.savedSuggestions[indexPath.row]
        view.setData(suggestedCity: obj, comesFrom: comesFrom)
    }
}

extension RealmService{
    func getSavedSuggestions() -> Results<SuggestedCityModel>{
        let results = realm.objects(SuggestedCityModel.self)
        
        return results
    }
    
    func getAddedCitiesToMain() -> Results<SuggestedCityModel>{
        let results = realm.objects(SuggestedCityModel.self)
        let predicate = NSPredicate(format: "isAddedToMain == \(true)")
        let addedToMainArr = results.filter(predicate)

        return addedToMainArr
    }

}


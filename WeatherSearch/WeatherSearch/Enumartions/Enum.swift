//
//  Enum.swift
//  WeatherSearch
//
//  Created by Asmaa Badreldin on 7/10/21.
//

import Foundation

enum ScreenNameEnum:Int {
    case main = 0
    case search = 1
}

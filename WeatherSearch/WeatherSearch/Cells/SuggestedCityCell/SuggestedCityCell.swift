//
//  SuggestedCityCell.swift
//  WeatherSearch
//
//  Created by Asmaa Badreldin on 7/10/21.
//

import UIKit

class SuggestedCityCell: UITableViewCell {

    var delegate:suggestedCellDelegate?
    @IBOutlet weak var cityNameLbl: UILabel!
    @IBOutlet weak var addBtn: UIButton!
    @IBOutlet weak var detailsTrailingConstaint: NSLayoutConstraint!
    @IBOutlet weak var containerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //set values in cell
    func setData(suggestedCity: SuggestedCityModel,comesFrom:Int) {
        
        switch comesFrom {
        case ScreenNameEnum.main.rawValue:
            addBtn.setTitle("remove", for: .normal)
        case ScreenNameEnum.search.rawValue:
            addBtn.setTitle("add", for: .normal)
        default:
            addBtn.setTitle("remove", for: .normal)
        }
        
        if let name = suggestedCity.name{
            self.cityNameLbl.text = name
        }
        
        if suggestedCity.isAddedToMain{
            if comesFrom == ScreenNameEnum.main.rawValue{
                self.addBtn.isHidden = false
                self.detailsTrailingConstaint.constant = 70
            }else{
                self.addBtn.isHidden = true
                self.detailsTrailingConstaint.constant = 0
                containerView.backgroundColor = UIColor.clear
            }
        }else{
            containerView.backgroundColor = UIColor.clear
            self.addBtn.isHidden = false
            self.detailsTrailingConstaint.constant = 70
        }
    }
    
    @IBAction func addToMainBtnHandler(_ sender: UIButton) {
        self.delegate?.addSuggestedCityToMain(cell: self, sender: sender)
    }
    
    @IBAction func suggestedTxtClicked(_ sender: UIButton) {
        self.delegate?.suggestCellIsclicked(cell: self, sender: sender)
    }

}

//MARK:-suggestedCellDelegate
protocol suggestedCellDelegate {
    func suggestCellIsclicked(cell:SuggestedCityCell,sender:UIButton)
    func addSuggestedCityToMain(cell:SuggestedCityCell,sender:UIButton)
}

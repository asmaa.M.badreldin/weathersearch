//
//  Forecastcell.swift
//  WeatherSearch
//
//  Created by Asmaa Badreldin on 7/9/21.
//

import UIKit

class Forecastcell: UITableViewCell {

    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var forecastLbl: UILabel!
    @IBOutlet weak var containerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        containerView.setCornerRadius(radius: 20)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //  set values in cell
    func setData(dayForecast: DaysForcast) {
        if let dateStr = dayForecast.dt_txt{
            self.dateLbl.text = dateStr
        }
        if let weather = dayForecast.weather?.first{
            self.forecastLbl.text = weather.description
        }
    }
}
